#!/usr/bin/env python
from PIL import Image
import sys

def convert(filename):
    image = Image.open(filename).convert("1")
    width, height = image.size
    imgs = []
    for y in range(0, height, 21):
        for x in range(0, width, 24):
            imgs.append(image.crop((x, y, x + 24, y + 21)))

    data = []
    for x in imgs:
        data += list(x.getdata())
        data += [0] * 8

    bitdata = []
    count = 0
    for d in data:
        if not count & 7:
            bitdata.append(0)
        bitdata[-1] <<= 1
        bitdata[-1] |= d & 1
        count += 1

    sys.stdout.write(''.join([chr(x) for x in bitdata]))

for arg in sys.argv[1:]:
    convert(arg)

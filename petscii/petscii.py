#! /usr/bin/env python

'''
Converts images to monochrome PETSCII character data
2012 boomlinde
'''

from PIL import Image
from sys import argv

def bitcount(c):
    count = 0
    while c:
        count += c & 1
        c >>= 1
    return count

btc = [bitcount(x) for x in range(256)]
chargen = [ord(x) for x in file("chargen").read()]

for arg in argv[1:]:
    print "Processing %s" % arg
    image = Image.open(arg)
    image = image.convert("L")
    image = image.resize((320, 200))
    a = list(image.getdata())
    threshold = sum(a) / len(a)
    image = image.point([int(x < threshold) * 255 for x in range(256)])
    image = image.convert("1")
    data = list(image.getdata())

    print "\tGenerating bitmap..."
    bitdata = []
    count = 0
    for d in data:
        if not count & 7:
            bitdata.append(0)
        bitdata[-1] <<= 1
        bitdata[-1] |= d & 1
        count += 1

    print "\tRe-arranging into char columns..."
    chardata = []
    for y in range(25):
        for x in range(40):
            offset = y * 320 + x
            for j in range(8):
                chardata.append(bitdata[offset + j * 40])

    print "\tFinding best charset matches..."
    petscii = []
    for i in range(1000):
        bestmatch = 1000
        matchchar = 0
        for char in range(256):
            match = 0
            for j in range(8):
                match += btc[chargen[char * 8 + j] ^ chardata[i * 8 + j]]
            if match < bestmatch:
                bestmatch = match
                matchchar = char
        petscii.append(matchchar)

    print "\tDone!"
    file(arg + ".chr", "w+").write(''.join([chr(x) for x in petscii]))
